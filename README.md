# Pruebas ORUSXPERT

El desarrollo de la aplicación consta de un backend hecho en DjangoRestFramework, en el cual se aloja 1 base de datos con 1 tabla La cual consta de 3 campos, nombre, latitud, longitud. Estos campos son utilizados para almacenar las posiciones geográficas que son enviadas desde el front


El front, esta implementado en angular  8.2.13, y     "node": "12.18.3","npm": "6.14.6"
para la presentación de los mapas, se usó GoogleMaps por medio del complemento npm i @agm/core@1.1.0, y la representación de rutas gracias a agm-direction, el resto del funcionamiento de la aplicación consta de conexiones entre el backend y el front por medio de sus 5 métodos básicos POST,GET,UPDATE,PATCH,DELETE


#para correr backend

cd backend-orusxpert-djangorf/ubicaciones/
python3 -m pip install django djangorestframework
python3 -m pip install django-cors-headers
python3 manage.py runserver

#para correr front

cd front-orusxpert-angular/
ng serve --poll=2000





