import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { CommonModule, CurrencyPipe} from '@angular/common';
import  {MatCurrencyFormatModule} from 'mat-currency-format';
import {IMaskModule} from 'angular-imask';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// components
import { AppComponent } from './app.component';
import { MapaComponent } from './components/mapa/mapa.component';
import { MapaEditarComponent } from './components/mapa/mapa-editar.component';

import { HomeComponent } from './components/home/home.component';

import { ConfirmationPopoverModule } from 'angular-confirmation-popover';


// routes
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


// servicios
import { UbicacionService } from './services/ubicacion.service';
import { CorreoService } from './services/correo.service';

//importacion de modulos
import { MaterialModule } from './material.module';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';



@NgModule({
  declarations: [    
    AppComponent,        
    HomeComponent, 
    MapaComponent, 
    MapaEditarComponent
  ],
  imports: [
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCs8l-6WLTK_5549-fvMj4vDYnR6RAlcfQ'
    }),
    AgmDirectionModule,
    BrowserModule,
    IMaskModule,
    CommonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,      
    MatCurrencyFormatModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger', // set defaults here
    }) 
  ],
  providers: [
    UbicacionService,
    CurrencyPipe,
    CorreoService
  ],
  entryComponents: [
    MapaEditarComponent
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}
