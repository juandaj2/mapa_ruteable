import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UbicacionModel } from '../models/ubicacion.model';
import { GeneralService } from './general.service';

import Swal from 'sweetalert2';


@Injectable()
export class UbicacionService {
    private url = 'ubicacion/api/';
    constructor(private http: HttpClient,
                private generalService: GeneralService) {
        this.url = generalService.genPath() + this.url;        
    }

    getUbicacion() {
        return this.http.get(this.url).pipe(
            map(resp => this.createArray(resp))
        );
    }

    getUbicacionById(id: number) {
        return this.http.get(this.url + `${id}/`);
    }

    postUbicacion(ubicacion: UbicacionModel) {
      return this.http.post(this.url, ubicacion);
    }

    patchtUbicacion(id:number,ubicacion: UbicacionModel) {
        return this.http.patch(this.url+`${id}/`, ubicacion);
    }

    putUbicacion(id:number,ubicacion: UbicacionModel) {
        return this.http.put(this.url+`${id}/`, ubicacion);
    }

    deleteUbicacion(id: number) {
    return this.http.delete(this.url + `${id}/`);        
    }

    private createArray(ubicacionesObj: object) {
        const ubicaciones: UbicacionModel[] = [];
        if (ubicacionesObj === null) {return []; }
    
        Object.keys(ubicacionesObj).forEach(key => {
            const ubicacion: UbicacionModel = ubicacionesObj[key];
            ubicaciones.push(ubicacion);
        });
    
        return ubicaciones;
    }

}