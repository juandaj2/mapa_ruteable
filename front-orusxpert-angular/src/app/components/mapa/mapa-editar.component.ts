import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder,FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-mapa-editar',
  templateUrl: './mapa-editar.component.html',
  styleUrls: ['./mapa-editar.component.css']
})
export class MapaEditarComponent implements OnInit {
  forma: FormGroup;

  constructor(
    public fb: FormBuilder,
    public dialogRef: MatDialogRef<MapaEditarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

      /*this.forma = new FormGroup({//Forma para primer punto 
        name: new FormControl(''),
        latitud: new FormControl('', Validators.required),
        longitud: new FormControl('', Validators.required)
      });*/

      console.log( data  );
      this.forma = fb.group({
        'id': data.id,
        'name': data.name,
        'latitud' : data.lat,
        'longitud' : data.lng
      });

  }

  ngOnInit() {
  }


  guardarCambios() {

    console.log(this.forma.value);
    this.dialogRef.close(this.forma.value);
    

  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
