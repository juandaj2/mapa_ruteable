import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Marcador } from '../../classes/marcador.class';
import { location } from '../../classes/waypoints.class';
import { UbicacionService } from '../../services/ubicacion.service';
import Swal from 'sweetalert2';

import { MatDialog, MatDialogRef } from '@angular/material';
import { MapaEditarComponent } from './mapa-editar.component';


@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  marcadores: Marcador[] = []; //Permite graficar los puntos con marker
  waypoints: location[] = []; //Define los puntos a integrar en medio de un P.inicio P.final
  
  
  /*lat = 4.5352546101533076; //Punto incial
  lng = -75.68166608636851;*/

  lat: number; //Punto incial
  lng: number;

  directions : boolean = false; //Actualiza la ruta

  origin ;  //Punto inicio
  destination; //Punto final

  public renderOptions = {  //Deshabilita las etiquetas del agm-directions
    suppressMarkers: true,
  }
  forma: FormGroup;
  inCname: string;
  conExion: boolean = true;
  forma2: FormGroup;
  punTosv: number = 0;
  id0_Ubicacion: number;  
  forma3: FormGroup;

  constructor(private ubicacionService: UbicacionService,
              public dialog: MatDialog) {    
    this.forma = new FormGroup({//Forma para primer punto 
      name: new FormControl(''),
      latitud: new FormControl('', Validators.required),
      longitud: new FormControl('', Validators.required)
    });
    this.forma2 = new FormGroup({//Forma para guardado de todos los puntos
      name: new FormControl('', Validators.required),
      latitud: new FormControl('', Validators.required),
      longitud: new FormControl('', Validators.required)
    });
    this.forma3 = new FormGroup({//Forma para actualizar datos
      id: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      latitud: new FormControl('', Validators.required),
      longitud: new FormControl('', Validators.required)
    });
  }

  //forma
  get latitudNovalido(){
    return this.forma.get('latitud').invalid && this.forma.get('latitud').touched
  }
  get longitudNovalido(){
    return this.forma.get('longitud').invalid && this.forma.get('longitud').touched
  }

  ngOnInit() {
    this.callPoints();
  }

  callPoints(){
    this.marcadores =[]; 
    this.waypoints =[];
    this.ubicacionService.getUbicacion().subscribe(resp =>{//Trae datos de django
      
      //this.id0_Ubicacion = resp[0].id;
      this.punTosv = resp.length;
      this.conExion=true;  //Establece sin conexión a la base de datos para evitar errores al guardar
      try {
        if(this.punTosv > 0){ //Comprueba que existan puntos
          this.id0_Ubicacion = resp[0].id;
          this.lat =  Number (resp[0].latitud); //Se define punto de partida para el mapa
          this.lng = Number (resp[0].longitud);
          this.forma.patchValue({name: resp[0].name});
          this.forma.patchValue({longitud: Number (resp[0].longitud)});
          this.forma.patchValue({latitud: Number (resp[0].latitud)});
          resp.forEach(element =>{ 
            
            const nuevoMarcador = new Marcador(Number(element.latitud),Number(element.longitud));
            nuevoMarcador.name=element.name;
            nuevoMarcador.id=element.id;

            this.marcadores.push( nuevoMarcador ); //Se agregar las coordenadas al arreglo de marcadores

            this.waypoints.push(//Se agregan los puntos existentes a la ruta de multiples puntos 
              {
                location:{
                  lat: Number(element.latitud),
                  lng: Number(element.longitud)
                }
              }
            );
          });

          console.log(this.marcadores);

          this.origin = { lat: Number(this.lat), lng: Number(this.lng) }; //Se establece el punto de origen de la ruta
          if (this.punTosv > 1) {//Si existen mas de 1 punto, se fija el destino y se traza la ruta

            var latitud = Number(resp.slice(-1)[0].latitud);
            var longitud = Number(resp.slice(-1)[0].longitud);
            this.inCname=resp.slice(-1)[0].name;
            this.destination = { lat: latitud, lng: longitud };//Se establece punto destino de la ruta
            this.load_route();
          }

        }else{//Si no existen puntos,se fija uno para inicializar el mapa
            this.lat = 4.5352546101533076; //Punto incial del mapa
            this.lng = -75.68166608636851;            
        }
      } catch (error) {
        Swal.fire({
          title: "Error",
          text:"Se presento un error al traer los datos",
          icon: "error"
        })
      }
    },error =>{
      this.lat = 4.5352546101533076; //Punto incial del mapa
      this.lng = -75.68166608636851;     
      this.conExion=false;  //Establece sin conexión a la base de datos para evitar errores al guardar
      Swal.fire({
        title: "Error",
        text:"Sin conexión a la base de datos, la información no sera guardada. Intente refresacando la página",
        icon: "error"
      })      
    });
  }

  agregarMarcador( evento ){ //Función para agregar marcadores y puntos a la ruta

    const coords: {lat: number, lng:number } = evento.coords; 
        
    if (this.marcadores.length < 1) {
      this.origin = { lat: coords.lat, lng: coords.lng }; //Se establece el punto de origen
      //vincular a la forma
      this.inCname="A" //Inicializa el nombre del primer punto
      this.forma.patchValue({name: this.inCname});
      this.forma.patchValue({longitud: coords.lng});
      this.forma.patchValue({latitud: coords.lat});          
    }else{
      //Se toma los nombres de los puntos que existen, y se incrementa
      //this.inCname <-- name del ultimo punto}
      var cName = String.fromCharCode(this.marcadores.slice(-1)[0].name.charCodeAt(0) + 1); //Aumenta el nombre del ultimo punto
      this.inCname = cName;

    }        
    
    this.GuardarPunto(this.inCname, coords.lng.toString() ,coords.lat.toString());
    
  }

  load_route()//Funcion para actualizar grafica
  {
    this.directions = false;
    setTimeout(() => {  //Timer para renovar la grafica de la ruta
      this.directions = true;
    }, 100); 
  }

  GuardarPunto(name: string, longitud: string, latitud:string){//Funcion para guardar puntos al hacer click en pantalla
    if (this.conExion) {
      this.forma2.patchValue({name: name});
      this.forma2.patchValue({longitud: longitud});
      this.forma2.patchValue({latitud: latitud});
      console.log(this.forma2.value);
      if (this.forma2.valid) {
        console.log("Guardando en base de datos");
        this.ubicacionService.postUbicacion(this.forma2.value).subscribe((resp: any)=>{
          
          var id = resp.id
          this.registroPuntos(name, Number(latitud) , Number(longitud), id)
          if(this.punTosv < 1 ){
            this.id0_Ubicacion = id;
          }
          this.punTosv++;
          
        },error=>{
          Swal.fire({
            title: "Error",
            text:"Se presento un error guardar los datos",
            icon: "error"
          });
        });
        
      } else {
        Swal.fire({
          title: "Datos errones",
          text:"Los datos no cumple los requerimientos",
          icon: "error"
        });
        
      }      
    }else{
      var id = null;
      this.registroPuntos(name, Number(latitud) , Number(longitud), id)
    }

  }

  registroPuntos(name: string, lat : number, lng:number, id_punto: number){    
    const nuevoMarcador = new Marcador ( lat, lng );    
    nuevoMarcador.name=name;
    nuevoMarcador.id=id_punto;
    this.marcadores.push( nuevoMarcador );        
    this.waypoints.push( 
      {
        location:{
          lat: lat,
          lng: lng
        }
      }
    );

    this.destination = { lat: lat, lng: lng };
    this.load_route();    
  }

  puntoInicial(){
    console.log(this.forma.value.name);
    if (this.punTosv < 1) {//Si se hace click en guardar y no existen puntos      
      //vincular a la forma
      this.inCname="A" //Inicializa el nombre del primer punto
      this.forma.patchValue({name: this.inCname});      
      console.log(this.forma.value,"post");
      if (this.forma.valid) {//Si la información es válida, post
        this.ubicacionService.postUbicacion(this.forma.value).subscribe(resp=>{
          console.log(resp);
          this.callPoints();
        },error =>{
          Swal.fire({
            title: "Error",
            text:"Se presento un error guardar los datos",
            icon: "error"
          });
        });
      } else {
        Swal.fire({
          title: "Formulario inválido",
          text:"El formulario no cumple los requerimientos",
          icon: "error"
        })
      }
    }else{
      //Se Actualiza la información PATCH, longitud, latitud
      console.log(this.forma.value,"patch");

      if (this.forma.valid) {//Si la información es válida, patch
        this.ubicacionService.patchtUbicacion(this.id0_Ubicacion,this.forma.value).subscribe(resp=>{
          console.log(resp);
          this.callPoints();
        },error =>{
          Swal.fire({
            title: "Error",
            text:"Se presento un error actualizando los datos",
            icon: "error"
          })
        });
      } else {
        Swal.fire({
          title: "Formulario inválido",
          text:"El formulario no cumple los requerimientos",
          icon: "error"
        })
      }

    }
  }

  borrar_punto(index_table:number,id_db ){//Funcion de borrado de puntos
    console.log(index_table,id_db);
    this.marcadores.splice(index_table,1); //Borra de la lista de marcadores
    this.waypoints.splice(index_table,1); //Borra de la lista de marcadores
    if (this.conExion) { //Si hay conexión con la base de datos, borra en DB
      this.ubicacionService.deleteUbicacion(id_db).subscribe(resp=>{
        console.log("OK,delete");
      },error=>{
        Swal.fire({
          title: "Error",
          text:"Se presento un error intentado borrar el registro",
          icon: "error"
        })
      });
    };

    var lat_origen = this.marcadores[0].lat;
    var lng_origen = this.marcadores[0].lng;

    var lat_destino = this.marcadores.slice(-1)[0].lat;
    var lng_destino = this.marcadores.slice(-1)[0].lng;
    
    this.origin = { lat: Number(lat_origen), lng: Number(lng_origen) }; //Se establece el punto de origen de la ruta
    this.destination = { lat: Number(lat_destino), lng: Number(lng_destino) };//Se establece punto destino de la ruta
    
  }

  editarMarcador(index_table: number,marcador : Marcador){

    const dialogRef = this.dialog.open( MapaEditarComponent , {
      width: '250px',
      data: { id:marcador.id, name: marcador.name, lat: marcador.lat, lng: marcador.lng }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      if ( !result ) {
        return;
      }
      console.log(result);
      marcador.name = result.name;
      marcador.lat = result.latitud;
      marcador.lng = result.longitud;
      marcador.id = result.id;
      console.log(marcador);
      
      /*Nota se debe editar en
          -waypoints
          -marcadores
          -base de datos
      */
      
      if(this.conExion){
        console.log("edita en base de datos Update");
        this.forma3.patchValue({id: result.id});
        this.forma3.patchValue({name: result.name});
        this.forma3.patchValue({latitud: result.latitud});
        this.forma3.patchValue({longitud: result.longitud});
        console.log(this.forma3.value);
        if (this.forma3.valid) {
          this.ubicacionService.putUbicacion(result.id, this.forma3.value).subscribe(resp=>{
            console.log("OK update",resp);
          },error=>{
            Swal.fire({
              title: "Error",
              text:"Se presento un error actualizando los datos",
              icon: "error"
            })            
          });
        } else {
          Swal.fire({
            title: "Error",
            text:"Los datos no cumplen los requerimientos",
            icon: "error"
          })
        }
      }
      this.waypoints[index_table].location.lat = Number(result.latitud);
      this.waypoints[index_table].location.lng = Number(result.longitud);
      console.log(this.waypoints[index_table]);

      var lat_origen = this.marcadores[0].lat;
      var lng_origen = this.marcadores[0].lng;
  
      var lat_destino = this.marcadores.slice(-1)[0].lat;
      var lng_destino = this.marcadores.slice(-1)[0].lng;
      
      this.origin = { lat: Number(lat_origen), lng: Number(lng_origen) }; //Se establece el punto de origen de la ruta
      this.destination = { lat: Number(lat_destino), lng: Number(lng_destino) };//Se establece punto destino de la ruta
      //this.load_route(); 
      //this.guardarStorage();
      //this.snackBar.open('Marcador actualizado', 'Cerrar', { duration: 3000 });

    });

  }

  rechagPagecontent(){
    
    this.ubicacionService.getUbicacion().subscribe((resp:any) =>{
      if (resp.length > 0) {
        resp.forEach(element => {
          this.ubicacionService.deleteUbicacion(element.id).subscribe(Cdel=>{
            console.log(Cdel);
          });          
        });
        window.location.reload();

      } else {
        window.location.reload();
      }
    });
  }

}
