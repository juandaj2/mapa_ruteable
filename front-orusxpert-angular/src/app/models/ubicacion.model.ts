export class UbicacionModel {
    id: number;
    name: string;
    latitud: string;
    longitud: string; 

    constructor() {
        this.id = null;
        this.name = null;
        this.latitud = null;
        this.longitud = null;
    }

}
