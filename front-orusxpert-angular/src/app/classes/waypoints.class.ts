export class location {
    location: {
        lat: number;
        lng: number;
    }

    constructor( lat: number, lng: number){
        this.location.lat = lat;
        this.location.lng = lng;
    }
}