export class Marcador {
    public lat: number;
    public lng: number;
    public id: number;
    public name: string = 'Sin nombre';

    constructor( lat: number, lng: number){
        this.lat = lat;
        this.lng = lng;
    }
}